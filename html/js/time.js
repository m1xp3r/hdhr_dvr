$(document).ready(async function () {
    async function updateTime() {
        $.ajax({
            url: 'index.php?time&ping',
            start_time: new Date().getTime(),
            success: function () {
                $.ajax({
                    url: 'index.php?time',
                    start_time: this.start_time,
                    success: async function (data) {
                        $("#current-time").html(data);
                        let rtt = (new Date().getTime() - this.start_time);
                        setTimeout(updateTime, 1000 - rtt);
                    }
                });
            }
        });
    }

    await updateTime();
});
