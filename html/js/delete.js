function confirmSoftDelete(url) {
    if (window.confirm("Are you sure you want to delete and re-record this episode?")) {
        fetch(url + '&cmd=delete&rerecord=1', {
            method: "POST",
        }).then(res => {
            console.log("Soft Delete complete! response:", res);
            window.location.reload();
        });
    }
}

function confirmDelete(url) {
    if (window.confirm("Are you sure you want to delete this recording FOREVER?")) {
        fetch(url + '&cmd=delete', {
            method: "POST",
        }).then(res => {
            console.log("Hard Delete complete! response:", res);
            window.location.reload();
        });
    }
}
