<?php
$TZ = getenv('TZ');
if ($TZ === false) {
    $TZ = 'UTC';
}
date_default_timezone_set($TZ);
$HDHR_IP = getenv('IP');
if ($HDHR_IP === false) {
    exit('{"error":"HDHR IP not set! Please set the IP environment variable."}');
} else {
    $JSON = file_get_contents('http://' . $HDHR_IP . '/discover.json');
    $HDHR_INFO = json_decode($JSON, true);
    $StorageURL = $HDHR_INFO['StorageURL'];
}

// Time
if (isset($_GET['time'])) {
    if (!isset($_GET['ping'])) {
        $date = date('l, d F Y H:i:s T');
        echo $date;
    } else {
        echo 'pong';
    }
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="3600">
    <meta name="viewport" content="width=device-width, initial-scale=2, user-scalable=1">
    <title>HDHR DVR Dashboard</title>

    <!-- CSS -->
    <link rel="preload" href="css/bootstrap/bootstrap.min.css" as="style"
          onload="this.onload=null;this.rel='stylesheet'">
    <link rel="preload" href="css/base.css" as="style"
          onload="this.onload=null;this.rel='stylesheet'">
    <noscript>
        <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/base.css" type="text/css">
    </noscript>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&display=swap"
          rel="stylesheet">

    <!-- Icons -->
    <link rel="apple-touch-icon" sizes="180x180" href="img/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/icons/favicon-16x16.png">
    <link rel="manifest" href="img/icons/site.webmanifest">
</head>
<body>
<div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand"><i class="fa-solid fa-hard-drive"></i> HDHR DVR</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav btn-group-lg">
                    <li class="nav-item">
                        <a class="nav-link btn btn-lg btn-light me-3" aria-current="page" href="index.php"><i
                                    class="fa-solid fa-video"></i> All Shows</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-lg btn-light me-3" href="?sort=news"><i
                                    class="fa-solid fa-newspaper"></i> News</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-lg btn-light me-3" href="?sort=series"><i class="fa-solid fa-tv"></i>
                            Series</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-lg btn-light me-3" href="?table"><i class="fa-solid fa-table"></i>
                            Table</a>
                    </li>
                </ul>
            </div>
            <div class='nav-time'>
                <div><i class="fa-solid fa-clock"></i> <span id="current-time"><small class="text-muted">Getting Time ...</small></span>
                </div>
            </div>
        </div>
    </nav>
    <?php
    if (isset($_GET['id'])) {
        $JSON = file_get_contents($StorageURL . '?SeriesID=' . $_GET['id']);
        $recordedFiles = json_decode($JSON, true);
        ?>
        <div class="row mt-3">
            <div class="col-2">
                <button class="btn btn-light btn-lg" onClick="window.history.back()"><i
                            class="fa-solid fa-backward"></i> <strong>Go Back</strong></button>
            </div>
            <div class="col-8">
                <h1 class="display-5"><?= $recordedFiles[0]['Title']; ?></h1>
            </div>
            <div class="col-2">
                <button class="btn btn-light btn-lg" onClick="window.location.reload()"><i
                            class="fa-solid fa-arrows-rotate"></i> <strong>Refresh</strong></button>
            </div>
        </div>
        <hr>
        <div class="row row-cols-1 row-cols-md-5 g-4 justify-content-center">
            <?php
            foreach ($recordedFiles as $file) {
                if ($file['RecordEndTime'] <= time()) {
                    $date = date('D, d M y', $file['RecordStartTime']);
                    $time = date('H:i', $file['RecordStartTime']);
                    ?>
                    <div class="col">
                        <div class="card h-100 bg-dark">
                            <?php if (isset($file['EpisodeTitle'])) { ?>
                                <div class="card-header">
                                    <h4 class="card-header"><?= $file['EpisodeTitle']; ?></h4>
                                    <h5 class="card-subtitle text-muted"><?= $file['EpisodeNumber']; ?></h5>
                                </div>
                            <?php } ?>
                            <a href="<?= $file['PlayURL']; ?>"><img src="<?= $file['ImageURL']; ?>"
                                                                    class="card-img-top"
                                                                    alt="<?= $file['Title']; ?>"
                                                                    title="<?= $file['Title']; ?>"></a>
                            <div class="card-body">
                                <h3 class="card-title"><?= $date; ?> <small class="text-muted">(<?= $time; ?>)</small>
                                </h3>
                                <?php if (isset($file['Synopsis'])) { ?>
                                    <hr class="hr">
                                    <p class="card-text"><?= $file['Synopsis']; ?></p>
                                    <hr>
                                <?php } ?>
                                <div class="row">
                                    <button type="button" id="play"
                                            onclick="window.location.href='<?= $file['PlayURL']; ?>';"
                                            class="btn btn-lg btn btn-success"><i class="fa-solid fa-play"></i>
                                        <strong>Play</strong>
                                    </button>
                                </div>
                                <div class="row mt-3">
                                    <div class="col">
                                        <button type="button" id="soft_delete"
                                                onclick="confirmSoftDelete('<?= $file['CmdURL']; ?>')"
                                                class="btn btn-warning btn-sm delete_button"><i
                                                    class="fa-solid fa-trash"></i> <strong>Delete & Re-Record</strong>
                                        </button>
                                    </div>
                                    <div class="col">
                                        <button type="button" id="delete"
                                                onclick="confirmDelete('<?= $file['CmdURL']; ?>')"
                                                class="btn btn-danger btn-sm delete_button"><i
                                                    class="fa-solid fa-trash-can"></i> <strong>Delete Forever</strong>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <?php
    } elseif (isset($_GET['table'])) {
        ?>
        <div class="row justify-content-center mt-3">
            <div class="col-auto text-center">
                <h1>All Recorded Shows</h1>
            </div>
        </div>
        <hr>
        <?php
        $JSON = file_get_contents($StorageURL);
        $shows = json_decode($JSON, true);
        foreach ($shows as $show) {
            $JSON = file_get_contents($StorageURL . '?SeriesID=' . $show['SeriesID']);
            $recordedFiles = json_decode($JSON, true);
            ?>
            <div class="row justify-content-left mt-3">
                <div class="col-auto">
                    <img class="img-thumbnail" height="80px" width="80px" src="<?= $show['ImageURL']; ?>">
                </div>
                <div class="col-auto">
                    <h2 class="display-5"><?= $show['Title']; ?></h2>
                </div>
            </div>
            <table class="table table-dark table-responsive table-striped mt-3">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Date</th>
                    <th scope="col">Meta</th>
                    <th scope="col">Synopsis</th>
                    <th scope="col">Play</th>
                    <th scope="col">Delete</th>
                </tr>
                </thead>
                <tbody class="table-group-divider">
                <?php
                $i = 1;
                foreach ($recordedFiles as $file) {
                    $date = date('D, d M y', $file['RecordStartTime']);
                    $time = date('H:i', $file['RecordStartTime']);
                    ?>
                    <tr>
                        <th scope="row"><?= $i; ?></th>
                        <td class="fw-bold"><?= $date . ' ' . $time; ?></td>
                        <td><?= (isset($file['EpisodeTitle'])) ? $file['EpisodeTitle'] . ' (' . $file['EpisodeNumber'] . ')' : 'N/A'; ?></td>
                        <td><?= (isset($file['EpisodeTitle'])) ? $file['Synopsis'] : 'N/A'; ?></td>
                        <td><a href="<?= $file['PlayURL']; ?>"><i class="fa-solid fa-play fa-2x text-success"></i></a>
                        </td>
                        <td><a type="button" id="delete" onclick="confirmDelete('<?= $file['CmdURL']; ?>')"><i
                                        class="fa-solid fa-trash text-danger fa-2x"></i></a></td>
                    </tr>
                    <?php
                    $i++;
                }
                ?>
                </tbody>
            </table>
            <hr>
            <?php
        }
    } else {
        ?>
        <div class="row row-cols-1 row-cols-md-6 g-4 mt-2 justify-content-center">
            <?php
            $JSON = file_get_contents($StorageURL);
            $recordedFiles = json_decode($JSON, true);
            foreach ($recordedFiles as $file) {
                if (isset($_GET['sort'])) {
                    if ($_GET['sort'] == 'news') {
                        if ($file['Category'] !== 'news') {
                            $skip = true;
                        } else {
                            goto GET_SERIES;
                        }
                    } elseif ($_GET['sort'] == 'series') {
                        if ($file['Category'] !== 'series') {
                            $skip = true;
                        } else {
                            goto GET_SERIES;
                        }
                    } else {
                        goto GET_SERIES;
                    }
                } else {
                    GET_SERIES:
                    ?>
                    <div class="col">
                        <div class="card card-catalogue h-100 bg-dark">
                            <?php if (isset($file['EpisodeTitle'])) { ?>
                                <div class="card-header">
                                    <h5 class="card-header"><?= $file['EpisodeTitle']; ?></h5>
                                    <strong class="card-subtitle text-muted"><?= $file['EpisodeNumber']; ?></strong>
                                </div>
                            <?php } ?>
                            <a href="?id=<?= $file['SeriesID']; ?>"><img class="img-thumbnail img-fluid"
                                                                         src="<?= $file['ImageURL']; ?>"
                                                                         alt="<?= $file['Title']; ?>"
                                                                         title="<?= $file['Title']; ?>"></a>
                            <div class="card-body">
                                <h6 class="card-title"><a
                                            href="?id=<?= $file['SeriesID']; ?>"><?= $file['Title']; ?></a></h6>
                            </div>
                        </div>
                    </div>
                <?php }
            } ?>
        </div> <?php } ?>
    <script src="js/jquery/jquery.min.js"></script>
    <script defer src="js/bootstrap/bootstrap.bundle.min.js"></script>
    <script defer src="js/fontawesome/all.min.js"></script>
    <script defer src="js/time.js"></script>
    <script defer src="js/delete.js"></script>
</body>
</html>
