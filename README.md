# HD HomeRun DVR Dashboard

This is a dashboard for the HD HomeRun DVR. It is a PHP web application that runs on a local web server.
It provides a simple interface to the HD HomeRun DVR that can be accessed from smartTV's and other devices.

## Usage

```bash
docker run --name DVR -p <LOCAL_PORT>:80 -e TZ='<LOCAL_TIMEZONE>' -e IP='<HDHR_IP_ADDRESS>' --rm -d maxtpower/hdhr_dvr
```

## Environment Variables

| Variable | Description                          | Default |
|----------|--------------------------------------|---------|
| `IP`     | The IP address of the HD HomeRun DVR | N/A     |
| `TZ`     | The timezone of the local machine    | UTC     |

## Tested Smart TV's

- Samsung Tizen (Plays mpg video via link)
