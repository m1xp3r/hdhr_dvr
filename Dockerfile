FROM php:8.1-apache

ARG BUILD_DATE
ARG VCS_REF

LABEL MAINTAINER="max@maxtpower.com" \
org.label-schema.schema-version="1.0" \
org.label-schema.build-date=${BUILD_DATE} \
org.label-schema.name="m1xp3r/hdhr_dvr" \
org.label-schema.description="HD HomeRun DVR Dashboard" \
org.label-schema.vcs-url="https://gitlab.com/m1xp3r/hdhr_dvr" \
org.label-schema.vcs-ref=${VCS_REF} \
org.label-schema.vendor="M1XP3R"

COPY /html /var/www/html
